<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

get_header(); ?>

		<?php
		if ( have_posts() ) : ?>
			<!-- <header class="page-header">
				<?php
					the_archive_title( '<h2 class="page-title">', '</h2>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header> -->
			<!-- .page-header -->

				<?php
					global $query_string;
//						query_posts( $query_string . 'orderby=title&order=DESC&posts_per_page=-1' );
					query_posts( $query_string . '&post_type=issue&orderby=title&order=DESC&posts_per_page=-1' );
				?>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'archive-issue' );

			endwhile;

			//the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

<?php
get_footer();
