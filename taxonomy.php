<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package mind
 */

get_header(); ?>
<div id="main-content">
  <main role="main">
<?php  //echo __FILE__; ?>
      <!-- issue title, volume, number, season, description, featured image -->
      <?php	get_template_part( 'template-parts/content-taxonomy-title');	?>

      <?php
      	global $query_string;
      	query_posts( $query_string . '&posts_per_page=-1' );
      ?>
      <?php
      /* Start the Loop */
			while ( have_posts() ) : the_post();	  ?>
        <?php	get_template_part( 'template-parts/content-taxonomy');	?>
      <?php  endwhile; ?>

	</main><!-- #main -->
</div><!-- #primary -->
<?php
//get_sidebar();
get_footer();
