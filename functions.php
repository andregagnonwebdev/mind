<?php
/**
 * MIND functions and definitions
 *
 * @package MIND
 */

define( 'LOCAL_IP_ADDR', '10.0.2.15');
define( 'DISALLOW_FILE_EDIT', true);

//error_log( 'test');
//$x = $y;

if ( ! function_exists( 'mind_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function mind_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on MIND, use a find and replace
	 * to change 'mind' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'mind', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'issue-list', 108, 140 ); // 220 pixels wide by 180 pixels tall, soft proportional crop mode
//	add_image_size( 'article-thumbnail', 320, 0 ); // 220 pixels wide by 180 pixels tall, soft proportional crop mode

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'mind' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'mind_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// custom logo
	add_theme_support( 'custom-logo', array(
		 'width'       => 900,
		 'height'      => 120,
	) );
}
endif; // mind_setup
add_action( 'after_setup_theme', 'mind_setup' );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
	 * @global int $content_width
	 */
	function mind_content_width() {
		$GLOBALS['content_width'] = apply_filters( 'mind_content_width', 640 );
	}
	add_action( 'after_setup_theme', 'mind_content_width', 0 );


/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function mind_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'mind' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'mind_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function mind_scripts() {

	if ( WP_DEBUG)
		$version = '4'.date( '.YmdGi'); // for DEBUG
	else
		$version = null;

	// WP required CSS
  wp_enqueue_style( 'mind-style-css', get_stylesheet_uri(), array(), $version ); // style.css

	// bootstrap css framework
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css-dist/bootstrap.min.css', array(), $version);
	wp_enqueue_style( 'font-awesome-css', 'https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), null);
	// site CSS
	$css = ( WP_DEBUG) ? '/css/main.css' : '/css-dist/main.min.css';
	wp_enqueue_style( 'main-css', get_template_directory_uri() . $css, array( 'mind-style-css', 'bootstrap-css'), $version);



	// load google fonts

	$fonts = mind_get_theme_fonts();
	global $mind_all_fonts;
	foreach ($fonts as $f) {
		if ( array_key_exists($f, $mind_all_fonts) &&
					$mind_all_fonts[ $f][ 1]) {
			$l = 'https://fonts.googleapis.com/css?family=';
			global $mind_all_fonts;
			$l .= str_replace( ' ', '+', $f) . ':' . $mind_all_fonts[ $f][ 1];
			$l .= '';
			wp_enqueue_style( 'google-font-'.str_replace( ' ', '-', $f), $l, array( 'mind-style-css', 'bootstrap-css'), null);
		}
	}

  	// move jquery to footer
  if (!is_admin()) {
        wp_deregister_script('jquery');
        // Load the copy of jQuery that comes with WordPress in to footer
        wp_register_script('jquery', '/wp-includes/js/jquery/jquery.js', false, $version, true);
        wp_enqueue_script('jquery');
	}

	// for watch in Gruntfile.js
	if ( in_array( $_SERVER['SERVER_ADDR'], array( '127.0.0.1', LOCAL_IP_ADDR)) || pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION) == 'dev') {
    	wp_enqueue_script( 'livereload', '//localhost:35729/livereload.js', '', false, true );
	}

	// use minified js
	$js = ( WP_DEBUG) ? 'bootstrap.js' : 'bootstrap.min.js';
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/'.$js, array('jquery'), $version, true);

	$js = ( WP_DEBUG) ? '/js/theme.js' : '/js/dist/theme.min.js';
  wp_enqueue_script( 'mind-theme-js', get_template_directory_uri() . $js, array('jquery'), $version, true );

	// new fontawesome
	//wp_enqueue_script( 'font-awesome-js', 'https://use.fontawesome.com/c88288cbb6.js', array(), $version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'mind_scripts' );

/**
 * Remove emoji support
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
//require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory() . '/inc/jetpack.php';
/**
 * Custom google fonts
 */
//require get_template_directory() . '/inc/google-font.php';

/**
 * Custom theme options
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Custom theme options
 */
//require get_template_directory() . '/inc/custom-theme-options.php';

/**
 * Utility functions
 */
// set up this global for some functions
$sep = '/';
$dl_filepath = dirname(dirname(dirname(dirname(__FILE__)))) . $sep . 'wp-content' . $sep . 'uploads';
require get_template_directory() . '/inc/util.php';

/**
 * Contact Form class
 */
//require get_template_directory() . '/inc/class.contact.php';

/**
 * Custom post types
 */
require get_template_directory() . '/post-types/issue.php';
require get_template_directory() . '/post-types/article.php';


/**
 * WooCommerce Support
 */
// Removes showing results in Storefront theme
//remove_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 20 );
//remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
// remove sort order in products display
//remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

//remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
//remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

//add_action('woocommerce_before_main_content', 'mind_theme_wrapper_start', 10);
//add_action('woocommerce_after_main_content', 'mind_theme_wrapper_end', 10);

function mind_theme_wrapper_start() {
    echo '<div id="primary" class="content-area">';
    echo '<div id="main" class="site-main col-xs-12 col-md-12" role="main">';

}

function mind_theme_wrapper_end() {
    echo '</div>';
    echo '</div>';
}

//add_action( 'after_setup_theme', 'mind_woocommerce_support' );
function mind_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


//
// add an editor to author taxonomies?
//
// remove the html filtering, for authors(?)
//remove_filter( 'pre_term_description', 'wp_filter_kses' );
//remove_filter( 'term_description', 'wp_kses_data' );

add_filter('authors_edit_form_fields', 'author_description');
function author_description($tag)
{
    ?>
        <table class="form-table">
            <tr class="form-field">
                <th scope="row" valign="top"><label for="description"><?php _ex('Description', 'Taxonomy Description'); ?></label></th>
                <td>
                <?php
                    $settings = array('wpautop' => true, 'media_buttons' => false, 'quicktags' => true, 'textarea_rows' => '15', 'textarea_name' => 'description' );
										//wp_editor( html_entity_decode(  $tag->description ), 'cat_description', $settings);
										wp_editor( html_entity_decode( wp_kses_post( $tag->description , ENT_QUOTES, 'UTF-8')), 'cat_description', $settings);
                ?>
                <br />
                <span class="description"><?php _e('The description is not prominent by default; however, some themes may show it.'); ?></span>
                </td>
            </tr>
        </table>
    <?php
}

add_action('admin_head', 'remove_default_author_description');
function remove_default_author_description()
{
    global $current_screen;
//echo '<div style="float:right">';var_dump( $current_screen);echo '</div>';

    if ( $current_screen->id == 'edit-authors' )
    {
    ?>
        <script type="text/javascript">
        jQuery(function($) {
            $('textarea#description').closest('tr.form-field').remove();
        });
        </script>
    <?php
    }
}


add_filter('artists_edit_form_fields', 'artist_description');
function artist_description($tag)
{
    ?>
        <table class="form-table">
            <tr class="form-field">
                <th scope="row" valign="top"><label for="description"><?php _ex('Description', 'Taxonomy Description'); ?></label></th>
                <td>
                <?php
                    $settings = array('wpautop' => true, 'media_buttons' => false, 'quicktags' => true, 'textarea_rows' => '15', 'textarea_name' => 'description' );
										//wp_editor( html_entity_decode(  $tag->description ), 'cat_description', $settings);
										wp_editor( html_entity_decode( wp_kses_post( $tag->description , ENT_QUOTES, 'UTF-8')), 'cat_description', $settings);
                ?>
                <br />
                <span class="description"><?php _e('The description is not prominent by default; however, some themes may show it.'); ?></span>
                </td>
            </tr>
        </table>
    <?php
}

add_action('admin_head', 'remove_default_artist_description');
function remove_default_artist_description()
{
    global $current_screen;
//echo '<div style="float:right">';var_dump( $current_screen);echo '</div>';

    if ( $current_screen->id == 'edit-artists' )
    {
    ?>
        <script type="text/javascript">
        jQuery(function($) {
            $('textarea#description').closest('tr.form-field').remove();
        });
        </script>
    <?php
    }
}



/**
 * ACF
 *
 */
 if( function_exists('acf_add_options_page') ) {
	 $args = array(

 	/* (string) The title displayed on the options page. Required. */
 	'page_title' => 'Options',

 	/* (string) The title displayed in the wp-admin sidebar. Defaults to page_title */
 	// 'menu_title' => '',

 	/* (string) The slug name to refer to this menu by (should be unique for this menu).
 	Defaults to a url friendly version of menu_slug */
 	 'menu_slug' => 'acf-options',

 	/* (string) The capability required for this menu to be displayed to the user. Defaults to edit_posts.
 	Read more about capability here: http://codex.wordpress.org/Roles_and_Capabilities */
 	'capability' => 'edit_posts',

 	/* (int|string) The position in the menu order this menu should appear.
 	WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
 	Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
 	Defaults to bottom of utility menu items */
 	'position' => 40,

 	/* (string) The slug of another WP admin page. if set, this will become a child page. */
 	'parent_slug' => '',

 	/* (string) The icon class for this menu. Defaults to default WordPress gear.
 	Read more about dashicons here: https://developer.wordpress.org/resource/dashicons/ */
 	'icon_url' => false,

 	/* (boolean) If set to true, this options page will redirect to the first child page (if a child page exists).
 	If set to false, this parent page will appear alongside any child pages. Defaults to true */
 	'redirect' => true,

 	/* (int|string) The '$post_id' to save/load data to/from. Can be set to a numeric post ID (123), or a string ('user_2').
 	Defaults to 'options'. Added in v5.2.7 */
 	'post_id' => 'options',

 	/* (boolean)  Whether to load the option (values saved from this options page) when WordPress starts up.
 	Defaults to false. Added in v5.2.8. */
 	'autoload' => false,

 );
 	acf_add_options_page( $args);
// 	acf_add_options_page();
 }

 // count number of articles belonging to this issue
 function mind_count_articles( $issue_post_id) {

 	// get issue term if this issue_post_id
	$terms = wp_get_post_terms( $issue_post_id, 'issues' );
	// var_dump( $terms[ 0]->slug);
	$issue_term = $terms[ 0]->slug;

	$exclude_list = array();
	$title_list = array(
		'1902_pitch',
		'2001_pitch',
		'2002_pitch',
		'2101_pitch',
		'2102_pitch',
		'2702_pitch',
		'2801_pitch',
		'2802_pitch',

	);

	foreach ( $title_list as $key => $title	) {
		$exclude_post = get_page_by_title( $title, OBJECT, 'article');
		if ( $exclude_post ) {
			$exclude_list[] = $exclude_post->ID;
		}
	}
	// $exclude_post = get_page_by_title( '2102_pitch-more-to-come', 'OBJECT', 'article');
	// if ( $exclude_post ) {
	// 	$exclude_list[] = $exclude_post->ID;
	// }
 	// get article post types with this issue term, count theme
	$args = array(
		'post_type' => 'article',
		'nopaging' => true,
		'post__not_in' => $exclude_list,
		'tax_query' => array(
			array(
				'taxonomy' => 'issues',
				'field'    => 'slug',
				'terms'    => $issue_term,
			),
		),
	);
	$q = new WP_Query( $args);

	// get num posts matching
	if ( 1 || WP_DEBUG) {
		$count = rand( 0, 15);
		$count = count( $q->posts);
		//$count = ( $count > 0) ? $count - 1 : 0;
	}
	else {
		$count = 0;
	}
	return( $count);
//	return( rand( 0, 15));

 }

 function mind_get_the_term_list( $id, $taxonomy) {
	 $s = '';
	 $terms = get_the_terms( $id, $taxonomy);
	 if ( $terms ) {

		 $term_array = array();
		 foreach ($terms as $key => $t) {
		 	if ($t->parent == 0) {
				$link = get_term_link( $t, $taxonomy );
				if ( is_wp_error( $link ) ) {
						 ;
				 }
				else
				 $term_array[] = '<a href="' . esc_url( $link ) . '" rel="tag">' . $t->name . '</a>';

		 	}
		 }
		 foreach ($terms as $key => $t) {
			if ($t->parent > 0) {
				$link = get_term_link( $t, $taxonomy );
				if ( is_wp_error( $link ) ) {
						 ;
				 }
				else
				 $term_array[] = '<a href="' . esc_url( $link ) . '" rel="tag">' . $t->name . '</a>';

			}
		 }
	 $s = implode( ' | ', $term_array);
 	}

	 return( $s);
 }
