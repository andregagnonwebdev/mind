<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package mind
 */

get_header(); ?>

<?php
if ( have_posts() ) : ?>

<div class="container-fluid">
	<div class="container">

		<div id="primary" class="content-area row">
			<div class="col-xs-12">
				<div class="entry-content row">

					<main id="main" class="site-main col-xs-12 col-md-12" role="main">

						<header class="page-header">
							<h2 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'mind' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
							<?php echo get_search_form( ) ?>
							&nbsp;<br />
						</header><!-- .page-header -->
					</main>
				</div>
			</div>
		</div>

	</div>
</div>

<?php
		global $query_string;
		//query_posts( $query_string . '&posts_per_page=-1' );
?>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			//the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
		</div>

<?php
//get_sidebar();
get_footer();
