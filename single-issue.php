<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package mind
 */

get_header(); ?>
<div id="main-content">
  <main role="main">
      <!-- issue title, volume, number, season, description, featured image -->
      <?php while ( have_posts() ) : the_post();    ?>
  		    <?php	get_template_part( 'template-parts/flexible-content-issue');	?>
      <?php  endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->
<?php
//get_sidebar();
get_footer();
