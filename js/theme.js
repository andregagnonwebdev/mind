/**
 * MIND
 * http://underscores.me
 *
 * Theme javascript
 */

(function ($) {
  'use strict';

  	// make wp_nav_menu compatible with bootstrap dropdowns
	$( "li.menu-item-has-children" ).addClass( "dropdown" );
	$( "li.menu-item-has-children > a").addClass( "dropdown-toggle" );
	$( "li.menu-item-has-children > a").attr('data-toggle', 'dropdown');
	$( "ul.sub-menu" ).addClass( "dropdown-menu" );
	$( "ul.sub-menu" ).addClass( "dropdown-menu-left" );

  // search
  $( ".search-glass a").click( function () {
		$(".search-container").toggle( 4);
    $(".search-form .search-field").focus();
	});

  // hide hr at the end of listed items
  // issue table of contents, article previous to a section heading
  $('.issue-section-heading').prev().find('hr').css('border-color', 'white');
  $('.issue-section-heading').prev().find('hr').css('background-color', 'white');

  // table of contents, article previous to the footer
  $('footer').prev().find('.issue-article-title:last').find('hr').css('border-color', 'white');
  $('footer').prev().find('.issue-article-title:last').find('hr').css('background-color', 'white');

  // issue list, last issue
  $('.post-type-archive-issue footer').prev().find('hr').css('border-color', 'white');
  $('.post-type-archive-issue footer').prev().find('hr').css('background-color', 'white');

  // add target blank
  $('.article-1-column .author a').attr( 'target', '_blank');

  // add id to artist-images
  $('.article-1-column img').attr( 'alt', function() {
      var alt =  $(this).attr("alt");
       //alert( alt);
       if ( alt == 'artist-image') {
         $(this).attr( 'id', alt );
       }
 });



})(jQuery);
