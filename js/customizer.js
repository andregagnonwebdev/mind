/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );




	wp.customize( 'mind-color-nav-link', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default .navbar-nav > li > a, .call-telephone a, .navbar-default .navbar-nav .current-menu-item a ' ).css( {
					'color': to
				} );
			} );
		} );

		wp.customize( 'mind-color-text-body', function( value ) {
			value.bind( function( to ) {
					$( 'body, #main' ).css( {
						'color': to
					} );
				} );
			} );


	wp.customize( 'mind-color-text-link', function( value ) {
		value.bind( function( to ) {
				$( '.site-footer a .fa' ).css( {
					'color': to
				} );

				$( '.navbar-default' ).css( {
	//					'border-color': to
				} );
				$( '.navbar-default .navbar-toggle .icon-bar' ).css( {
					'background-color': to
				} );

		} );
	} );
	wp.customize( 'mind-color-text-link', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default .navbar-nav > li:hover' ).css( {
					'backgroundColor': to
				} );
		} );
	} );

	wp.customize( 'mind-color-text-link', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default .navbar-nav > li > a:hover' ).css( {
					'color': to
				} );
				$( '.site-main a, .site-main a:visited, .site-main a:active, .site-main a:hover, .site-main a:focus' ).css( {
					'color': to
				} );
		} );
	} );

	wp.customize( 'mind-color-background', function( value ) {
		value.bind( function( to ) {
				$( '.container-fluid, .site-main' ).css( {
					'background-color': to,
				} );
		} );
	} );

	// footer text color
	wp.customize( 'mind-color-text-footer', function( value ) {
		value.bind( function( to ) {
				$( '.site-footer' ).css( {
					'color': to,
				} );
		} );
	} );
	// footer link
	wp.customize( 'mind-color-text-footer-link', function( value ) {
		value.bind( function( to ) {
				$( '.site-footer a, .site-footer a .fa' ).css( {
					'color': to,
				} );
		} );
	} );
	// footer rollover
	wp.customize( 'mind-color-text-footer-rollover', function( value ) {
		value.bind( function( to ) {
				$( '.site-footer a:focus, .site-footer a:hover, .site-footer a:active, .site-footer a .fa:focus, .site-footer a .fa:hover, .site-footer a .fa:active' ).css( {
					'color': to,
				} );
		} );
	} );


	// misc
	wp.customize( 'mind-color-rule-lines', function( value ) {
		value.bind( function( to ) {
				$( 'hr' ).css( {
					'border-color': to
				} );
		} );
	} );
	wp.customize( 'mind-color-call-to-action', function( value ) {
		value.bind( function( to ) {
				$( '.page .container .call-to-action' ).css( {
					'background-color': to
				} );
		} );
	} );


	function mind_bind_html ( theme_mod) {
		wp.customize( theme_mod, function( value ) {
			value.bind( function( to ) {
				var class_name = '.' + theme_mod;
				$( class_name ).html( to );
			} );
		} );
	}

	function mind_bind_href ( theme_mod) {
		wp.customize( theme_mod, function( value ) {
			value.bind( function( to ) {
				var class_name = 'a.' + theme_mod;
				$( class_name).attr( { 'href' : to});
			} );
		} );
	}

	function mind_bind_email ( theme_mod) {
		wp.customize( theme_mod, function( value ) {
			value.bind( function( to ) {
				var class_name = 'a.' + theme_mod;
				var mailto = 'mailto:'+to;
				$( class_name).attr( { 'href' : mailto});
				$( class_name).html(to);
			} );
		} );
	}


	mind_bind_href ( 'mind-facebook');

	// email
//	mind_bind_email ( 'mind-email');

	// html
	mind_bind_html ( 'mind-copyright-message');

	// logo
	wp.customize( 'mind-img-upload', function( value ) {
		value.bind( function( to ) {
				$( '.mind-img-upload > img' ).attr( { 'src': to});
		} );
	} );


	// font
	wp.customize( 'mind-font-nav-menu', function( value ) {
		value.bind( function( to ) {
				$( '.navbar-default .navbar-nav > li > a' ).css( { 'font-family': to});
//				$( 'input[type="submit"]' ).css( { 'font-family': to});

		} );
	} );

	// fonts
	wp.customize( 'mind-font-body-text', function( value ) {
		value.bind( function( to ) {
				$( 'body' ).css( { 'font-family': to});
				$( '#main' ).css( { 'font-family': to});
				$( '.site-header, .site-main' ).css( { 'font-family': to});
		} );
	} );
	wp.customize( 'mind-font-heading', function( value ) {
		value.bind( function( to ) {
				$( 'h1, h2, h3, h4, h5, h6' ).css( { 'font-family': to});
		} );
	} );
	wp.customize( 'mind-font-nav-menu', function( value ) {
		value.bind( function( to ) {
				$( '.navbar' ).css( { 'font-family': to});
		} );
	} );

	wp.customize( 'mind-font-button', function( value ) {
		value.bind( function( to ) {
				$( '.button' ).css( { 'font-family': to});
		} );
	} );

	wp.customize( 'mind-font-caption', function( value ) {
		value.bind( function( to ) {
				$( '.caption' ).css( { 'font-family': to});
		} );
	} );


	wp.customize( 'mind-font-footer', function( value ) {
		value.bind( function( to ) {
				$( '.site-footer' ).css( { 'font-family': to});
		} );
	} );


} )( jQuery );
