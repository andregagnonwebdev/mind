<?php
/**
 * MIND Custom Widgets
 *
 * @package MIND
 */

class Custom_Blog_Post extends WP_Widget {
	function __construct() {
		parent::__construct( false, 'MIND Blog Post');
	}

function form($instance) {

		if ( isset( $instance[ 'title' ] ) )
		{
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'mind' );
		}

		// outputs the options form on admin
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'mind' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		<br /></p>
		<?php
	}

function update($new_instance, $old_instance) {

		// processes widget options to be saved
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
//		$instance['body'] = $new_instance['body'];

		return $instance;
	}

function widget($args, $instance) {
		// outputs the content of the widget

        $title = $instance['title'];
//        $body = $instance['body'];

        $s = '';

				$template_url = home_url();
        $upload_url = home_url() . '/wp-content/uploads/';

        ?>
	<hr class="home" />
	<?php /* Start the Loop */


        // Custom Image_Text_Link script
        //$s .=
        //'<div id="secondary" class="widget-area custom-sidebar col-xs-12 col-sm-12 col-md-12" role="complementary">
				//'<div class="textwidgetx mind-custom-postx">';

        if ( $title)
            $s .= '<a href="blog"><h2>'.$title.'</h2></a>';

        $args = array( 'post_type'=>'post',
                       'nopaging'=> true,
                       'orderby' => 'date', 'order' => 'DESC'  );
        $the_query = new WP_Query( $args);
        echo $s;
        $s = '';

        // just show the first one.
        if ( $the_query->have_posts() )
        	if ($the_query->have_posts())
        	{
        	    $the_query->the_post();
  //              $s .= '<h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
//                $s .= wpautop( get_the_content()).'<br />';
        	}


					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', 'excerpt-home' );


        //$s .= '</div>';
//        $s .= '</div><br />';

  //      echo $s;
	}


}
register_widget('Custom_Blog_Post');

?>
