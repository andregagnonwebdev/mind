<?php
/**
 * MIND contact form.
 *
 * @package MIND
 */

/*
Need to include this near <head>, put in functions.php?
<script src='https://www.google.com/recaptcha/api.js'></script>
*/

global $captcha;
$captcha = 1;

if ( $captcha)
{
    // recaptcha v2 from google
    global $publickey;
    global $privatekey;

    $publickey = "6LeM0xkTAAAAAFc-NCaF5CJn8sGOepa7nQ1uFiKG";
    $privatekey = "6LeM0xkTAAAAAC48SskZ_AOcTRVNCp6V3EVjqbL6";
}


/* Contact data  */

define( 'FIELD', 0);define( 'DATA_TYPE', 1);define( 'VALIDATE', 2);define( 'LABEL', 3);
define( 'IN_FORM', 4);define( 'REQUIRED', 5);define( 'ORDER', 6);define('PLACEHOLDER', 7);

global $defineContactFields;
$defineContactFields = array(

    // bookkeeping
    array( 'first_name', 'VARCHAR(255)', 'text',  'first name', '1', 1, 10),
    array( 'last_name', 'VARCHAR(255)', 'text',  'last name', '1', 1, 20),

    array( 'eps_title', 'VARCHAR(255)', 'text',  'title', '1', 1, 30),
    array( 'email',     'VARCHAR(255)', 'email', 'email', '1', 1, 35),
    array( 'company', 'VARCHAR(255)', 'text',  'company name', '1', 1, 37),
    array( 'service_title', 'VARCHAR(255)', 'label',  'Do you want to learn more about our performance management services?', '1', 1, 38),

    array( 'service_wo', 'BOOL', 'bool',  'Workshops', '1', 0, 40),
    array( 'service_el', 'BOOL', 'bool',  'e-Learning', '1', 0, 50),
    array( 'service_tr', 'BOOL', 'bool',  'Train-the-Trainer', '1', 0, 51),
    array( 'service_pa', 'BOOL', 'bool',  'Participant Training Materials', '1', 0, 52),
    array( 'service_we', 'BOOL', 'bool',  'Webinars', '1', 0, 53),
    array( 'service_co', 'BOOL', 'bool',  'Consulting', '1', 0, 54),
    array( 'service_sp', 'BOOL', 'bool',  'Speaking', '1', 0, 55),

    array( 'phone', 'VARCHAR(255)', 'text',  'phone', '1', 0, 80),
    array( 'message',   'TEXT',         'big_text', "We're always curious about why people are interested in this topic.<br /><b>Please share some quick thoughts</b>", '1', 0, 90),

    array( 'id',        'INT',          'id',  '', '', 0, 1000),   // id has to be last
);



// Contact data for html/php/mysql
class Contact
    {
    // fields of data values
    var $fValue;

    // validate error message
    var $fErrorMsg;
    var $captchaErrorMsg;

    // required field
    var $id;

    var $tableName; // mysql table
    var $debug;

    // constructor: called with 'new'
    function Contact()
        {
        global $wpdb;
        global $defineContactFields;

        $this->id = 0;
        $size = count( $defineContactFields);
        $this->fValue = array();
        $this->fErrorMsg = array();
        $this->captchaErrorMsg = '';
        $this->tableName  = $wpdb->prefix . "custom_contact_table";

        global $defineContactFields;
        foreach ( $defineContactFields as $def)
            {
            $field = $def [ FIELD];
            $type = $def[ DATA_TYPE];

            if ( $field != 'id')
                {
                if ( $type == 'INT' || $type == 'BOOL')
                    $this->fValue[ $field] = 0;  // init value
                else if ( $type == 'DATETIME')
                    $this->fValue[ $field] = GetGMTDateTime( 0);
                else
                    $this->fValue[ $field] = '';  // init value
                }
            else
                $this->fValue[ 'id'] = 0;  // init value

            }

            $this->debug = 1;
            $this->debug = 0;
        }


    function Define( $field, $val)
        {
        global $wpdb;

        if ( $field)
            $this->fValue[ $field] = $val;  // init value

        // special case
        if ( $field == 'id')
            $this->id = $val;

//        $this->tableName  = $wpdb->prefix . $table;

if ( $this->debug)
    $this->Dump();
        }

     //
     //
     // Model Functions

     // create table
     function CreateTable( $db)
     {
        // drop table
        // we're going to rebuild each time, so delete it
        $qry = 'DROP TABLE ' . $this->tableName;
        $res = $db->Query( $qry);

        // build table
        $qry = "CREATE TABLE  $this->tableName ( ";

        global $defineContactFields;
        foreach ( $defineContactFields as $def)
             {
             $field = $def [ FIELD];
             $data_type = $def [ DATA_TYPE];

             if ( $field != 'id')
                 {
                 $qry .= "$field $data_type, ";
                 }
             }
         $qry .= " id INT(10) NOT NULL AUTO_INCREMENT, PRIMARY KEY( id))";
         $res = $db->Query( $qry);

     }


     // does this exist
    function Exists( $db, $id)
        {
        $qry = "SELECT id FROM $this->tableName WHERE id='$id'";
        $res = $db->Query( $qry);
        if ( $res && $db->Size( $res) > 0)
            return( true);
        else
            {

if ( $this->debug)
    echo "<br />id: $id does not exist.";

            return( false);
            }
        }


     // get data using id
    function Get( $db)
        {
        // find in db, fill data fields

        if ( false == $this->Exists( $db, $this->id))
            {
if ( $this->debug)
    echo "<br />Get failed, id:$this->id does not exist.";

            return( false);
            }
        $qry = "SELECT * FROM $this->tableName WHERE id='$this->id'";
        $res = $db->Query( $qry);
        if ( $data = $db->Fetch( $res))
            {
            global $defineContactFields;
            foreach ( $defineContactFields as $def)
                {
                $field = $def [ 0];
                $this->fValue[ $field] = $data[ $field];
                }

            }

        return( true);
        }

     //  add this to the db
     function Add( $db)
        {

        // insert
if ( $this->debug)
    echo "<br />Adding $this->id";

        $qry = "INSERT INTO $this->tableName VALUES( ";
        global $defineContactFields;
        foreach ( $defineContactFields as $def)
            {
            $field = $def [ FIELD];
            if ( $field != 'id')
                {
                // for HTML, special characters
                if ( $def[ DATA_TYPE] != 'INT')
                    $val = StrEncode( $this->fValue[ $field]);
                else
                    $val = $this->fValue[ $field];

                // TBD: check for integers
                if ( $def[ DATA_TYPE] == INT)
                    $qry .= "$val, ";
                else
                    $qry .= "'$val', ";
                }
            }
        $qry .= "NULL )";

        $res = $db->Query( $qry);

        // get id
        $id = mysql_insert_id();
        $id = ( false ? 0 : $id);
        $this->id = $this->fValue[ 'id'] = $id;

        return( true);

     }

     //  put data into the db
     function Put( $db)
        {
        // if exists, update db
        if ( true == $this->Exists( $db, $this->id))
            {

            $qry = "UPDATE $this->tableName SET ";

            global $defineContactFields;
            foreach ( $defineContactFields as $def)
                {
                $field = $def [ 0];

                if ( $field != 'id')
                {
                // for HTML, special characters
                if ( $def[ 1] != 'INT')
                    $val = StrEncode( $this->fValue[ $field]);
                else
                    $val = $this->fValue[ $field];

                $qry .= $field ."='$val', ";
                }
                else
                {
                    $val = $this->fValue[ $field];
                    $qry .= $field ."='$val' ";
                }
                }

            $qry .= "WHERE id='$this->id'";
            $res = $db->Query( $qry);

            }
        else
            {
            // new
            $this->Add( $db);
            }

        // add property, community, utilities

        return( true);
        }

     function Delete( $db)
        {
        // delete
        $id = $this->id;

        $qry = "DELETE FROM $this->tableName WHERE id='$id'";
        $res = $db->Query( $qry);

        return( true);
        }


    // return an array list of ids
    function GetAll( $db, &$list, $where='1', $sort='')
        {

        $qry = "SELECT id FROM $this->tableName WHERE $where $sort";
        $res = $db->Query( $qry);
        while ( $data = $db->Fetch( $res))
            {
            $list[] = $data[ 'id'];
            }
        }



     //
     //
     // Controller functions

     // open-create
     function Open( $db, $id=0)
        {
        $this->id = $id;

        // add to database
        $this->Put( $db);

        }

     function Update( $db)
        {
if ( $this->debug)
    echo "<br />updating ...";

        // add to, update database
        $this->Put( $db);
        }

     function Save( $db)
        {
if ( $this->debug)
    $this->Dump();
if ( $this->debug)
    echo "<br />Save ...";

        // add to, update database
        $this->Put( $db);
        }


    function GetInputSave()
        {

        // get 'id' elsewhere

        global $defineContactFields;
        foreach ( $defineContactFields as $def)
            {
            $field = $def [ FIELD];
            $type = $def [ DATA_TYPE];

            if ( $type == 'BOOL')
                {
                GetPostInputCheckbox( $temp, $field);
                $this->fValue[ $field] = StrDecode( $temp);
                }
            else if ( $field != 'id')
                {
                GetPostInput( $temp, $field);
                $this->fValue[ $field] = StrDecode( $temp);
                }


            $temp = '';
            }
if ( $this->debug)
    $this->Dump();
        }


    // validate user input
    function Validate( $db, &$msg)
        {
        $flag = true;
        $msg = '';
        $prev = '';

        $i = 0;
        global $defineContactFields;
        foreach ( $defineContactFields as $def)
        {
            $field = $def[ FIELD];
            $type = $def[ DATA_TYPE];
            $validate = $def[ VALIDATE];
            $val = $this->fValue[ $field];
            $in_form = $def[ IN_FORM];
            $required = $def[ REQUIRED];

            if ( $in_form && $required)
            {
                if ( $validate == 'text')
                {
                    if( ValidateTextInput( $val, $this->fErrorMsg[ $field], $in_form) == false)
                    {
                        $flag = false;
                    }

                }
                if ( $validate == 'big_text')
                {
                    if( ValidateBigTextInput( $val, $this->fErrorMsg[ $field], $in_form) == false)
                    {
                        $flag = false;
                    }

                }
                if ( $validate == 'email')
                {
                    if( ValidateEmailInput( $val, $this->fErrorMsg[ $field]) == false)
                    {
                        $flag = false;
    //echo "<br />e:" . $this->fErrorMsg[ $field];
                    }
                }
                if ( $validate == 'phone')
                {
                    if( ValidatePhoneInput( $val, $this->fErrorMsg[ $field]) == false)
                    {
                        $flag = false;
    //echo "<br />e:" . $this->fErrorMsg[ $field];
                    }
                }

                if ( $validate == 'int')
                {
                    if( ValidateIntegerInput( $val, $this->fErrorMsg[ $field]) == false)
                    {
                        $flag = false;
                    }
                }
            }

            $i++;
        }

//$this->Dump();

        // check captcha
        if ( $flag)
        {
            global $captcha;
            if ( $captcha)
            {
                // was there a reCAPTCHA response?
                global $publickey;
                global $privatekey;

    //            print_rs( $_POST);

                if ( $_POST["g-recaptcha-response"])
                {
                  // TBD: USE  wp_remote_get() here
                     $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$privatekey."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
                     if($response.success==false)
                     {
                        $this->captchaErrorMsg = "reCaptcha error.";
                        $flag = false;
                     }else
                     {
                        $msg = '';
                     }
                }
                else
                {
                    $this->captchaErrorMsg = "reCaptcha error.";
                    $flag = false;
                }
            }
        }
        return( $flag);
        }



     // clear out data
     function Clear()
        {
        $this->id = 0;

        global $defineAgentFields;
        foreach ( $defineAgentFields as $def)
            {
            $field = $def[ 0];

            if ( $def[ 1] == 'INT')
                $this->fValue[ $field] = 0;
            else
                $this->fValue[ $field] = '';
            }

        }


     function SendMail ()
     {

    global $defineContactFields;
    // re-order
    $fieldOrder = array();
    foreach ( $defineContactFields as $def)
        $fieldOrder[] = $def [ ORDER];
    asort( $fieldOrder);

            $body = "Contact     \n\r";

    foreach ( $fieldOrder as $key => $index)
    {
        $def = $defineContactFields[ $key]; // point to def in order

        $field = $def [ FIELD];
        $val = $this->fValue[ $field];
        $type = $def [ DATA_TYPE];
        $valid = $def [ VALIDATE];
        $desc = $def [ LABEL];
        $in_form = $def [ IN_FORM];
        $required = $def [ REQUIRED];
        $order = $def [ ORDER];

        if ( $type == 'BOOL')
        {
            $val = ( $val) ? "Yes" : "No";
            if ( $val == "Yes")
                $body .= "$desc: $val \n\r";
        }
        else if ( $field != 'id')
        {
            if ( $field == 'message')
                $body .= "$desc: \n\r  $val \n\r";
            else
                $body .= "$desc: $val \n\r";

        }
    }


        $subject = 'Contact';
        $email_to = 'jamie@employeeperformancesolutions.com';
        $email_to = get_option( 'eps-email');
        if ( $email_to)
            SendEmail( $email_to, $subject, $body);

        $email_to = 'contact@andregagnon.com';
        SendEmail( $email_to, $subject, $body);

     }


     //
     //
     // View Functions

     // dump the data
     function Dump()
        {

        echo"<br />class Contact";

        global $defineContactFields;
        foreach ( $defineContactFields as $def)
            {
            $field = $def [ 0];
            $val = $this->fValue[ $field];
            echo "<br /> $field: $val ";
            $val = $this->fErrorMsg[ $field];
            if ( $val)
                echo "<br /> <span style='color:red'>error: $val </span>";
            }

        echo"<br />";

        }

     // display the fields, HTML/CSS
     function DisplayFields( $divClass='')
        {
        $str = '';

        global $defineRouteFields;
        foreach ( $defineRouteFields as $def)
            {
            $field = $def[ 0];

                {
                $val = $field;
                $str .= " $val, ";
                }
            }

        return( $str);
        }


     // display a single element in line form, HTML/CSS
     function DisplayLine( )
        {
        $str = '
';

        $str .= '<div style="width:630px;float:left;"> ';

        global $defineContactFields;
        foreach ( $defineContactFields as $def)
        {
            $field = $def [ FIELD];
            $val = $this->fValue[ $field];
            $type = $def [ DATA_TYPE];
            $valid = $def [ VALIDATE];
            $desc = $def [ LABEL];
            $in_form = $def [ IN_FORM];


//            if ( $in_form)
            {
                // fix up
                $val = StrHTMLEntities( $val);

                if ( $valid == 'text' || $valid == 'email')
                {
                    $str .= <<<DMAB
                    {$val} |
DMAB;
                }
            }
        }


        $str .= "</div>&nbsp;<br />"; // close container

        return( $str);
        }


    // form for user data entry
    function FormEdit2( $db)
    {

        $str = '';


//        $str .= '<span class="text-primary">* (denotes required field)</span><br /><br />
        $str .= '<br /><br />
        <form role="form " class="contact-form" method="POST">';


        $i = 0;
        global $defineContactFields;
        foreach ( $defineContactFields as $def)
            {
            $field = $def [ FIELD];
            $val = $this->fValue[ $field];
            $type = $def [ DATA_TYPE];
            $valid = $def [ VALIDATE];
            $desc = $def [ LABEL];
            $in_form = $def [ IN_FORM];
            $required = $def [ REQUIRED];
            $error = $this->fErrorMsg[ $field];
            //$error = '';
            if ( $error == '')
                $error = '';
            else
                $error = '<span class="text-danger">'.$error.'</span>';
            $has_error = $error ? 'has-error' : '';

            $req = ( $required ? '<span class="text-primary">*</span>' : '');
            $req = ( $required ? '*' : '');

            if ( $in_form)
                {
                // fix up
                $val = StrHTMLEntities( $val);

                if ( $valid == 'text' || $valid == 'email' || $valid == 'tel')
                {

                    $str .= <<<FE221
                    <div class="form-group {$has_error}">
                    <!--<label for="id{$field}">{$req}<span style="">{$desc}</span></label>&nbsp;&nbsp;-->{$error}
                    <input class="form-control"  id="id{$field}" type="{$valid}" name="{$field}" value="{$val}" placeholder="{$desc}{$req}" />
                    </div>
FE221;
                }

                if ( $valid == 'label')
                {

                    $str .= <<<FE221x
                    <div class="form-group {$has_error}">
                    <!--
                    <input class="form-control"  id="id{$field}" type="" name="{$field}" value="{$val}" placeholder="{$desc}" />
                    -->
                    <label class="eps-label" >{$desc}</label>
                    </div>
FE221x;
                }


                if ( $type == 'BOOL')
                {
                    // fix up
                    $val = StrHTMLEntities( $val);

                    if ( $val)
                        $checked = 'checked="checked"';
                    else
                        $checked = '';


                    $str .= <<<FE224

                    <div class="form-group {$has_error}">
                    <input class="form-control form-checkbox" id="id{$field}" type="checkbox" name="{$field}" value="{$val}" {$checked}  placeholder="{$desc}{$req}">
                    <label for="id{$field}" class="form-checkbox">{$req}<span style="">{$desc}</span></label>&nbsp;&nbsp;{$error}
                    </div>
FE224;

                }


                if ( $valid == 'big_text')
                {
                    $str .= <<<FE223
                    <div class="form-group {$has_error}">
                    <label class="eps-label" for="id{$field}">{$desc}:</label>
                    <textarea class="form-control" id="eps-form-text-area"  name="{$field}" rows="6"  placeholder="" >{$val}</textarea>
                    </div>
                    <br />
FE223;

                }


                }
            }


        global $captcha;
        if ( $captcha)
        {
            // captcha
            global $publickey;
            $str .= '<span class="text-danger"">' . $this->captchaErrorMsg . "</span><br />";
            $str .= '<div class="g-recaptcha" data-theme="clean" data-sitekey="'.$publickey.'" style="transform:scale(0.77);transform-origin:0;-webkit-transform:scale(0.77);transform:scale(0.77);-webkit-transform-origin:0 0;transform-origin:0 0; 0"></div>';

        }


        $url = home_url();

        $str .= '&nbsp;<br />';
        $str .= '&nbsp;<br />';
        $button_file = get_template_directory_uri().'/images/eps-button-send-message.png';
        $str .= <<<FE222
            <input classx="btn btn-primary pull-left"  type="image" src="{$button_file}" alt="Submit">
                    <input type="hidden" name="action" value="save_contact" />

        </form>
        &nbsp;<br />
        &nbsp;<br />
        &nbsp;<br />
FE222;

        return( $str);

     }



    }   // end class


?>
