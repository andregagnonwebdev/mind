<?php

if ( function_exists( 'add_theme_support' ) )
    {

        add_action('admin_menu', 'mind_theme_menu');
    }

function mind_theme_menu()
{
    add_theme_page('MIND Theme Options', 'MIND Theme', 'manage_options', 'nmm-unique-identifier', 'mind_theme_options');
}

function mind_theme_options()
{
        if (!current_user_can('manage_options'))  {
            wp_die( __('You do not have sufficient permissions to access this page.', 'mind') );
        }
        echo '<div class="wrap">';


        // variables for the field and option names
            $hidden_field_name = 'submit_hidden';

            $option_list = array(

            'mind-title1',
            'mind-title2',
            'mind-tagline',
            'mind-address1',
            'mind-town_state_zip',
            'mind-hours-days-open',
            'mind-telephone',
            'mind-mobile',
            'mind-email',
            'mind-copyright',

            'mind-facebook',
            'mind-twitter',
            //'mind-instagram',
            //'mind-youtube',
            'mind-linkedin',
            //'mind-googleplus',

            //'mind-hashtags',

            //'mind-google-fonts',
            'mind-mailchimp-url',
            'mind-mailchimp-u',
            'mind-mailchimp-id',
            );

            $option_title = array(

            'Company Name',
            'Company Name2',
            'Tagline',
            'Address',
            'Town, State  ZIPCODE',
            'Hours, Days Open',
            'Office Phone',
            'Mobile Phone',
            'Contact Email',
            'Copyright Text',

            'Facebook URL (e.g https://facebook.com/name)',
            'Twitter URL (e.g https://twitter.com/name)',
            //'Instagram URL (e.g https://instagram.com/name)',
            //'You Tube URL (e.g https://youtube.com/user/name)',
            'LinkedIn URL (e.g https://linkedin.com/name)',
            //'Google+ URL (e.g https://plus.google.com/u/0/+name/posts)',
            //'Hashtags (e.g. #hash, #tag)',
            //'Google Fonts (e.g. FontName:400,700). Each font a separate line.',
            'MailChimp URL',
            'MailChimp U',
            'MailChimp ID',

            );


            $opt_names = array();
            $field_names = array();
            $opt_vals = array();
            foreach ( $option_list as $key => $option)
            {
                $opt_names[] = $field_names[] = $option;
                $opt_vals[] =  get_option( $option );
                $opt_titles[] =  $option_title[ $key];
            }

            // See if the user has posted us some information
            // If they did, this hidden field will be set to 'Y'
            if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) {

                foreach ( $option_list as $key => $option)
                {
                    $opt_vals[ $key] =  $_POST[ $option ];
                    update_option( $option, $opt_vals[ $key] );
                }

                // Put an settings updated message on the screen

        ?>
        <div class="updated"><p><strong><?php _e('settings saved.', 'mind' ); ?></strong></p></div>
        <?php

            }


            // Now display the settings editing screen

            echo '<div class="wrap">';

            // header

            echo "<h2>" . __( 'MIND Theme Settings', 'mind' ) . "</h2>";

            // settings form

            ?>

        <form name="form1" method="post" action="">
        <input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">

<?php
        foreach ( $option_list as $key => $option)
        {
            if ( strstr( $option, 'mind-title1'))
                echo "<h3>Contact Information</h3>";
            if ( strstr( $option, 'facebook'))
                echo "<h3>Social Media</h3>";
            if ( strstr( $option, 'google-font'))
                echo "<h3>Google Fonts</h3>";
            if ( strstr( $option, 'mailchimp-url'))
                echo "<h3>MailChimp</h3>";

            if ( strstr( $option, 'google-font'))
            {
                echo "<label>" . $opt_titles[ $key] . ': &nbsp;</label><br />';
                echo '<textarea type="text" name="' . $field_names[ $key] . '"  cols="50" rows="8">'.$opt_vals[ $key].'</textarea>';
            }
            else if ( strstr( $option, '-image'))
            {
                // build select box

                $select = '<select name="' . $field_names[ $key] . '">';
                // get images
                $upload_list = GetSelectListDirUploads();
                $upload_images = explode( '|', $upload_list);

                $checked = ( '' ==  $opt_vals[ $key]) ? 'selected="selected"' : '';
                $select .= '<option value="" ' . $checked . ' >None</option>';
                // for each image
                foreach( $upload_images as $img )
                {
                    if ( $img ==  $opt_vals[ $key])
                        $checked = 'selected="selected"';
                    else
                        $checked = '';
                    $select .= '<option value="' . $img . '" ' . $checked . ' >' . $img . '</option>';
                }
                $select .= '</select>';

                echo "<p>" . $opt_titles[ $key] . ':';
                echo $select;
                echo "</p>";
            }
            else if ( strstr( $option, 'application'))
            {
                echo "<label>" . $opt_titles[ $key] . ': &nbsp;</label><br />';
                echo '<textarea type="text" name="' . $field_names[ $key] . '"  cols="50" rows="3">'.$opt_vals[ $key].'</textarea>';
            }
            else
            {
                echo "<p>" . $opt_titles[ $key] . ': &nbsp;';
                echo '<input type="text" name="' . $field_names[ $key] . '" value="' . $opt_vals[ $key] . '" size="30">';
                echo "</p>";
            }
        }

?>


        <hr />

        <p class="submit">
        <input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes', 'mind') ?>" />
        </p>


        </form>
        </div>

        <?php


        echo '</div>';

}


?>
