<?php
/**
 * MIND Theme Customizer
 *
 * @package MIND
 */


function mind_theme_mod_default( $key)
{
    static $mind_theme_mod_defaults = array(
      'mind-color-nav-link' => '#ca6929',
      'mind-color-text-heading' => '#000000',
      'mind-color-text-body' => '#000000',
      'mind-color-text-link' => '#ca6929',
      'mind-color-rule-lines' => '#cecece',
      'mind-color-call-to-action' => '#365684',

      'mind-color-background-body' => '#ffffff',

      'mind-font-nav-menu' =>  'Open Sans',
      'mind-font-heading' =>  'Roboto Condensed',
      'mind-font-body-text' =>  'Open Sans',
      'mind-font-caption' =>  'Open Sans',
      'mind-font-footer' =>  'Open Sans',


      'mind-facebook' => '',

      'mind-company-name' => '',
      'mind-address' => '',
      'mind-town-state-zip' => '',
      'mind-telephone' => '',
      'mind-email' => '',

      'mind-copyright-message' => '© Copyright 1984-2017. All rights reserved.',

      'mind-img-upload' => '',   // footer

    );

    if ( array_key_exists($key, $mind_theme_mod_defaults) )
        return( $mind_theme_mod_defaults[ $key]);
    else
    {
        return( '');
    }
}

function mind_get_theme_mod( $key, $default='unused')
{
    // provide defaults for 2nd parameter
    return( get_theme_mod( $key, mind_theme_mod_default( $key)));
}

// return all google fonts used in theme options, used in functions.php
function mind_get_theme_fonts() {

    $fonts = array();

    $fonts[] = mind_get_theme_mod( 'mind-font-header');
    $fonts[] = mind_get_theme_mod( 'mind-font-nav-menu');
    $fonts[] = mind_get_theme_mod( 'mind-font-title');
    $fonts[] = mind_get_theme_mod( 'mind-font-heading');
    $fonts[] = mind_get_theme_mod( 'mind-font-body-text');
    $fonts[] = mind_get_theme_mod( 'mind-font-button');
    $fonts[] = mind_get_theme_mod( 'mind-font-caption');
    $fonts[] = mind_get_theme_mod( 'mind-font-footer');
    $fonts = array_unique( $fonts);

    return( $fonts);
}

/////////////////////////////
// new font stuff



static $mind_all_fonts = array(
   'Georgia' => array( 'serif', ''),
   'Arial' => array( 'sans-serif', ''),

   // google
   'Arvo'  => array( 'serif', '400,400i,700,700i'),
   'Cardo'  => array( 'serif', '400,400i,700,700i'),
   'EB Garamond' => array( 'serif', '400,400i,700'),
   'Gentium Basic'  => array( 'serif', '400,400i,700,700i'),
   'Libre Baskerville' => array( 'serif', '400,400i,700'),
   'Neuton' => array( 'serif', '400,400i,700'),

   'Lato'  => array( 'sans-serif', '400,400i,700,700i'),
   'Open Sans'  => array( 'sans-serif', '400,400i,700,700i'),
   'Work Sans'  => array( 'sans-serif', '400,400i,700,700i'),
   'Open Sans Condensed'  => array( 'sans-serif', '300'),
   'Raleway'  => array( 'sans-serif', '400,400i,700,700i'),
   'Roboto'  => array( 'sans-serif', '400,400i,700,700i'),
   'Roboto Condensed'  => array( 'sans-serif', '400,400i,700,700i'),
   'Roboto Slab'  => array( 'sans-serif', '400,400i,700,700i'),

 );

 static $mind_body_text_fonts = array(
   'Georgia' => array( 'serif', ''),
   'Arial' => array( 'Helvetica, sans-serif', ''),

   // google
   'Arvo'  => array( 'serif', '400,400i,700,700i'),
   'EB Garamond' => array( 'serif', '400,400i,700'),
   'Gentium Basic'  => array( 'serif', '400,400i,700,700i'),
   'Libre Baskerville' => array( 'serif', '400,400i,700'),
   'Neuton' => array( 'serif', '400,400i,700'),

   'Lato'  => array( 'sans-serif', '400,400i,700,700i'),
   'Open Sans'  => array( 'sans-serif', '400,400i,700,700i'),
   'Work Sans'  => array( 'sans-serif', '400,400i,700,700i'),
   'Raleway'  => array( 'sans-serif', '400,400i,700,700i'),
   'Roboto'  => array( 'sans-serif', '400,400i,700,700i'),

 );

 // Add Google fonts CSS to admin header
 function mind_load_fonts() {

 global $mind_all_fonts;

 }

 // load fonts for admin?
 //add_action('customize_controls_enqueue_scripts', 'stout_oak_load_fonts');
 //add_action('wp_enqueue_scripts', 'mind_load_fonts');
 add_action('wp_default_scripts', 'mind_load_fonts');
 //add_action('admin_enqueue_scripts', 'mind_load_fonts');

 // Add Google font CSS to admin header
 function mind_font_family( $key) {

     global $mind_all_fonts;
     return( "'".$key."'".", ".$mind_all_fonts[ $key][0]);
}




/////////////////////////////
// old font stuff
function mind_google_fonts_filter( $f)
{
//    loco_print_r( $f);
    // remove some fonts
    return( $f['category'] != 'handwriting');
}

function mind_get_google_fonts( $max = 5, $all = true)
{
  $googleAllFontList = array();
  global $mind_all_fonts;
  global $mind_body_text_fonts;

  $fonts = ($all ?  ($mind_all_fonts):($mind_body_text_fonts));
  foreach( $fonts as $key => $f) {
    $googleAllFontList[ $key] = $key;
  }
  return( $googleAllFontList);
}

function mind_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function mind_customize_register( $wp_customize ) {
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
 //   $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

    // remove some stuff
    $wp_customize->remove_control( 'header_textcolor' );
    $wp_customize->remove_panel( 'widgets');
    $wp_customize->remove_section( 'static_front_page');

    // TBD make this data driven, for here and CSS below.

    ///////////////////////////////////////////////////////////////////////////
    // Colors
    $wp_customize->add_setting( 'mind-color-nav-link',
        array(
        'type' => 'theme_mod',
        'default' => mind_theme_mod_default( 'mind-color-nav-link'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mind-color-nav-link',
        array(
        'label' => __( 'Navigation Menu', 'mind_textdomain' ),
        'section' => 'colors',
    ) ) );


    $wp_customize->add_setting( 'mind-color-text-body',
        array(
        'type' => 'theme_mod',
        'default' => mind_theme_mod_default( 'mind-color-text-body'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mind-color-text-body',
        array(
        'label' => __( 'Body Text', 'mind_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'mind-color-text-link',
        array(
        'type' => 'theme_mod',
        'default' => mind_theme_mod_default( 'mind-color-text-link'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mind-color-text-link',
        array(
        'label' => __( 'Link', 'mind_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'mind-color-text-footer',
        array(
        'type' => 'theme_mod',
        'default' => mind_theme_mod_default( 'mind-color-text-footer'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mind-color-text-footer',
        array(
        'label' => __( 'Footer Text', 'mind_textdomain' ),
        'section' => 'colors',
    ) ) );


    $wp_customize->add_setting( 'mind-color-rule-lines',
        array(
        'type' => 'theme_mod',
        'default' => mind_theme_mod_default( 'mind-color-rule-lines'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mind-color-rule-lines',
        array(
        'label' => __( 'Rule Lines', 'mind_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'mind-color-call-to-action',
        array(
        'type' => 'theme_mod',
        'default' => mind_theme_mod_default( 'mind-color-call-to-action'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mind-color-call-to-action',
        array(
        'label' => __( 'Call to Action Button', 'mind_textdomain' ),
        'section' => 'colors',
    ) ) );


    $wp_customize->add_setting( 'mind-color-background',
        array(
        'type' => 'theme_mod',
        'default' => mind_theme_mod_default( 'mind-color-background'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mind-color-background',
        array(
        'label' => __( 'Background', 'mind_textdomain' ),
        'section' => 'colors',
    ) ) );

    /*
    $wp_customize->add_setting( 'mind-xxx',
        array(
        'type' => 'theme_mod',
        'default' => mind_theme_mod_default( 'mind-xxx'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mind-xxx',
        array(
        'label' => __( 'XXX Color', 'mind_textdomain' ),
        'section' => 'colors',
    ) ) );
    */


    ///////////////////////////////////////////////////////////////////////////
    // Fonts

    $fontListAll = mind_get_google_fonts( 25);
    $fontListBodyText = mind_get_google_fonts( 25, false);

    // section
    $wp_customize->add_section( 'mind-font-setting', array(
            'title'          => 'Fonts',
            'priority'       => 20,
            'description' => '<b>' . __( 'Select fonts.', 'mind' ) .'</b>' ,
        ) );

    // setting/control
    $wp_customize->add_setting( 'mind-font-nav-menu', array(
            'type'    => 'theme_mod',
            'default' => mind_theme_mod_default('mind-font-nav-menu'),
    //            'transport' => 'postMessage',
            'transport' => 'refresh',
        ) );

    $wp_customize->add_control( 'mind-font-nav-menu', array(
            'label' => 'Navigation Menu',
            'section' => 'mind-font-setting',
            'settings'   => 'mind-font-nav-menu',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'mind-font-heading', array(
            'type'    => 'theme_mod',
            'default' => mind_theme_mod_default('mind-font-heading'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'mind-font-heading', array(
            'label' => 'Heading',
            'section' => 'mind-font-setting',
            'settings'   => 'mind-font-heading',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'mind-font-body-text', array(
            'type'    => 'theme_mod',
            'default' => mind_theme_mod_default('mind-font-body-text'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'mind-font-body-text', array(
            'label' => 'Body Text',
            'section' => 'mind-font-setting',
            'settings'   => 'mind-font-body-text',
            'type'    => 'select',
            'choices' => $fontListBodyText,
    ) );

    $wp_customize->add_setting( 'mind-font-caption', array(
            'type'    => 'theme_mod',
            'default' => mind_theme_mod_default('mind-font-caption'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'mind-font-caption', array(
            'label' => 'Caption',
            'section' => 'mind-font-setting',
            'settings'   => 'mind-font-caption',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'mind-font-footer', array(
            'type'    => 'theme_mod',
            'default' => mind_theme_mod_default('mind-font-footer'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'mind-font-footer', array(
            'label' => 'Footer',
            'section' => 'mind-font-setting',
            'settings'   => 'mind-font-footer',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );


    /*
    $wp_customize->add_setting( 'mind-font-xxx', array(
            'type'    => 'theme_mod',
            'default' => 'arial,helvetica',
            'transport' => 'postMessage',
    ) );

    $wp_customize->add_control( 'mind-font-xxx', array(
            'label' => 'Button Font',
            'section' => 'mind-font-setting',
            'settings'   => 'mind-font-xxx',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    */


    ///////////////////////////////////////////////////////////////////////////
    // Social Media
    $wp_customize->add_section( 'mind-social-media-settings', array(
        'title'          => 'Social Media Settings',
        'priority'       => 160,
        'description' => '<b>' . __( 'Social media links.' ) .'</b>' ,
    ) );


    $wp_customize->add_setting( 'mind-facebook', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'mind_sanitize_text',
    ) );
    $wp_customize->add_control( 'mind-facebook', array(
        'label' => __( 'Facebook URL', 'loco_textdomain' ),
        'section' => 'mind-social-media-settings',
        'settings'   => 'mind-facebook',
        'type'    => 'text',
    ) );


    ///////////////////////////////////////////////////////////////////////////
    // Footer

    $wp_customize->add_section( 'mind-copyright-message-settings', array(
            'title'          => 'Contact Information',
            'priority'       => 200,
    ) );


    $wp_customize->add_setting( 'mind-company-name', array(
            'type'    => 'theme_mod',
            'default' => 'test',
            'transport' => 'postMessage',
            'sanitize_callback' => 'mind_sanitize_text',
        ) );
    $wp_customize->add_control( 'mind-company-name', array(
            'label' => 'Company Name',
            'section' => 'mind-copyright-message-settings',
            'settings'   => 'mind-company-name',
            'type'    => 'text',
        ) );

    $wp_customize->add_setting( 'mind-address', array(
            'type'    => 'theme_mod',
            'default' => '',
            'transport' => 'postMessage',
            'sanitize_callback' => 'mind_sanitize_text',
        ) );
    $wp_customize->add_control( 'mind-address', array(
            'label' => 'Address',
            'section' => 'mind-copyright-message-settings',
            'settings'   => 'mind-address',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'mind-town-state-zip', array(
            'type'    => 'theme_mod',
            'default' => '',
            'transport' => 'postMessage',
            'sanitize_callback' => 'mind_sanitize_text',
        ) );
    $wp_customize->add_control( 'mind-town-state-zip', array(
            'label' => 'town-state-zip',
            'section' => 'mind-copyright-message-settings',
            'settings'   => 'mind-town-state-zip',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'mind-telephone', array(
            'type'    => 'theme_mod',
            'default' => mind_theme_mod_default('mind-telephone'),
            'transport' => 'postMessage',
            'sanitize_callback' => 'mind_sanitize_text',
    ) );
    $wp_customize->add_control( 'mind-telephone', array(
            'label' => 'Telephone',
            'section' => 'mind-copyright-message-settings',
            'settings'   => 'mind-telephone',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'mind-copyright-message', array(
            'type'    => 'theme_mod',
            'default' => mind_theme_mod_default('mind-copyright-message'),
            'transport' => 'postMessage',
            'sanitize_callback' => 'mind_sanitize_text',
        ) );
    $wp_customize->add_control( 'mind-copyright-message', array(
            'label' => 'Copyright Text',
            'section' => 'mind-copyright-message-settings',
            'settings'   => 'mind-copyright-message',
            'type'    => 'text',
    ) );


}
add_action( 'customize_register', 'mind_customize_register' );


function mind_customize_css()
{
    // add customizer CSS to <head>
    ?>


    <style type="text/css">

        .navbar-default .navbar-nav > li > a {
          color: <?php echo mind_get_theme_mod( 'mind-color-nav-link', '#fff' ); ?>;
          font-family: <?php echo mind_font_family( mind_get_theme_mod( 'mind-font-nav-menu', 'arial,helvetica')); ?>;
        }
        .navbar-default .navbar-nav > li > a:hover {
            color: <?php echo mind_get_theme_mod( 'mind-color-text-link', '#aaa' ); ?>;
        }

        .navbar-default .navbar-nav .current-menu-item a {
            color: <?php echo mind_get_theme_mod( 'mind-color-nav-link', '#aaa' ); ?>;
        }

        .navbar .navbar-header button .icon-bar {
          /*background-color: <?php echo mind_get_theme_mod( 'mind-color-text-link', '#aaa' ); ?>;*/
        }
        .navbar .navbar-header button .icon-title {
          color: <?php echo mind_get_theme_mod( 'mind-color-text-link', '#aaa' ); ?>;
        }

        body, #main {
            color: <?php echo mind_get_theme_mod( 'mind-color-text-body', '#000' ); ?>;
            font-family: <?php echo mind_font_family( mind_get_theme_mod( 'mind-font-body-text', 'arial,helvetica')); ?>;
        }

        .site-header, .site-main {
          font-family: <?php echo mind_font_family( mind_get_theme_mod( 'mind-font-body-text', 'arial,helvetica')); ?>;
        }

        a, a:visited, a:hover, .site-main a, .site-main a:visited, .site-main a:active, .site-main a:hover, .site-main a:focus {
          color: <?php echo mind_get_theme_mod( 'mind-color-text-link', '#aaa' ); ?>;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: <?php echo mind_font_family( mind_get_theme_mod( 'mind-font-heading', 'arial,helvetica')); ?>;
        }

        .call-to-action {
          background-color: <?php echo mind_get_theme_mod( 'mind-color-call-to-action'); ?>;
        }
        input[type="submit"], button  {
            font-family: <?php echo mind_font_family( mind_get_theme_mod( 'mind-font-nav-menu', 'arial,helvetica')); ?>;
        }

        hr {
          border-color: <?php echo mind_get_theme_mod( 'mind-color-rule-lines', '#aaa' ); ?>;
        }

        .wp-caption, .caption {
            font-family: <?php echo mind_font_family( mind_get_theme_mod( 'mind-font-caption', 'arial,helvetica')); ?>;
        }

        .site-footer {
          font-family: <?php echo mind_font_family( mind_get_theme_mod( 'mind-font-footer', 'arial,helvetica')); ?>;
        }
    </style>
    <?php

}
add_action( 'wp_head', 'mind_customize_css');


// change editor_style CSS dynamically for the admin editor
function mind_theme_editor_dynamic_styles( $mceInit ) {
    $styles = " \
    body.mce-content-body {  \
      color:".mind_get_theme_mod( 'mind-color-text-body', '#000' )."; \
      font-family:".mind_font_family( mind_get_theme_mod( 'mind-font-body-text', 'arial,helvetica'))."; \
      font-size: 16px; \
    } \
    body.mce-content-body h1, h2, h3, h4, h5, h6 { \
      font-family:".mind_font_family( mind_get_theme_mod( 'mind-font-heading', 'arial,helvetica'))."; \
    } \
    .wp-caption-dd { \
      color:".mind_get_theme_mod( 'mind-body-textcolor', '#000' )."; \
    } \
    ";

    if ( isset( $mceInit['content_style'] ) ) {
        $mceInit['content_style'] .= ' ' . $styles . ' ';
    } else {
        $mceInit['content_style'] = $styles . ' ';
    }
    return $mceInit;
}
add_filter('tiny_mce_before_init','mind_theme_editor_dynamic_styles');


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function mind_customize_preview_js() {
    wp_enqueue_script( 'mind_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '4'.date( '.YmdGi'), true );
}

add_action( 'customize_preview_init', 'mind_customize_preview_js' );
