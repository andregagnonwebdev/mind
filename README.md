
Theme mind
================

WordPress Theme based on `_s`, or `underscores`; with `Bootstrap` as a CSS framework.

Notes
-----

### Description

A custom Yoeman generator is used to build the `_s` Theme with SASS/Bootstrap.

_Twitter_ **Bootstrap** classes are added to HTML elements in the **PHP** templates.

	Steps:

	* Install WordPress
	* Set Theme options, add pages, menus to WordPress with WP-CLI
	* Build _s Theme with SASS, Bootstrap
	* Add Bootstrap to Theme templates, modify files ... with shell scripts.
	* Run Grunt

Test theme.

	Start adding CSS to main.css, modify templates, add custom post types.
	
All done!

[andregagnon.com](http://andregagnon.com)

