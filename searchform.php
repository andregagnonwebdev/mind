<?php
/*
 * Search form
 *
 * @package mind
 */
?>

<div class="form">
 <form class="form-inline search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
   <div class="form-group">
		 <span class="screen-reader-text">Search for:</span>
     <input type="search" class="form-control search-field" id="search" placeholder="Search &hellip;" value="<?php echo is_search()? get_search_query() : ''; ?>" name="s">
   </div>
	 	<button type="submit" class="btn btn-default search-submit"><i class="fa fa-search"></i></button>
 </form>
</div>
