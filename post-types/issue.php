<?php

function issue_init() {
	register_post_type( 'issue', array(
		'labels'            => array(
			'name'                => __( 'Issues', 'mind' ),
			'singular_name'       => __( 'Issue', 'mind' ),
			'all_items'           => __( 'All Issues', 'mind' ),
			'new_item'            => __( 'New Issue', 'mind' ),
			'add_new'             => __( 'Add New', 'mind' ),
			'add_new_item'        => __( 'Add New Issue', 'mind' ),
			'edit_item'           => __( 'Edit Issue', 'mind' ),
			'view_item'           => __( 'View Issue', 'mind' ),
			'search_items'        => __( 'Search Issues', 'mind' ),
			'not_found'           => __( 'No Issues found', 'mind' ),
			'not_found_in_trash'  => __( 'No Issues found in trash', 'mind' ),
			'parent_item_colon'   => __( 'Parent Issue', 'mind' ),
			'menu_name'           => __( 'Issues', 'mind' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'menu_position'			=> 25,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-book',
		'show_in_rest'      => true,
		'rest_base'         => 'issue',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'issue_init' );

function issue_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['issue'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Issue updated. <a target="_blank" href="%s">View Issue</a>', 'mind'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'mind'),
		3 => __('Custom field deleted.', 'mind'),
		4 => __('Issue updated.', 'mind'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Issue restored to revision from %s', 'mind'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Issue published. <a href="%s">View Issue</a>', 'mind'), esc_url( $permalink ) ),
		7 => __('Issue saved.', 'mind'),
		8 => sprintf( __('Issue submitted. <a target="_blank" href="%s">Preview Issue</a>', 'mind'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Issue scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Issue</a>', 'mind'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Issue draft updated. <a target="_blank" href="%s">Preview Issue</a>', 'mind'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'issue_updated_messages' );
