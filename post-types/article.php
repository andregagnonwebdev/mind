<?php

function article_init() {
	register_post_type( 'article', array(
		'labels'            => array(
			'name'                => __( 'Articles', 'mind' ),
			'singular_name'       => __( 'Article', 'mind' ),
			'all_items'           => __( 'All Articles', 'mind' ),
			'new_item'            => __( 'New Article', 'mind' ),
			'add_new'             => __( 'Add New', 'mind' ),
			'add_new_item'        => __( 'Add New Article', 'mind' ),
			'edit_item'           => __( 'Edit Article', 'mind' ),
			'view_item'           => __( 'View Article', 'mind' ),
			'search_items'        => __( 'Search Articles', 'mind' ),
			'not_found'           => __( 'No Articles found', 'mind' ),
			'not_found_in_trash'  => __( 'No Articles found in trash', 'mind' ),
			'parent_item_colon'   => __( 'Parent Article', 'mind' ),
			'menu_name'           => __( 'Articles', 'mind' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'menu_position'			=> 25,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-page',
		'show_in_rest'      => true,
		'rest_base'         => 'article',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'article_init' );

function article_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['article'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Article updated. <a target="_blank" href="%s">View Article</a>', 'mind'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'mind'),
		3 => __('Custom field deleted.', 'mind'),
		4 => __('Article updated.', 'mind'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Article restored to revision from %s', 'mind'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Article published. <a href="%s">View Article</a>', 'mind'), esc_url( $permalink ) ),
		7 => __('Article saved.', 'mind'),
		8 => sprintf( __('Article submitted. <a target="_blank" href="%s">Preview Article</a>', 'mind'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Article scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Article</a>', 'mind'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Article draft updated. <a target="_blank" href="%s">Preview Article</a>', 'mind'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'article_updated_messages' );
