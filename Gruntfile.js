// Gruntfile.js
// for WordPress Theme development
module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'css/sass',
          src: ['*.scss'],
          dest: 'css',
          ext: '.css'
        }]
      },
      bootstrap: {
        files: [{
          expand: true,
          cwd: 'bower_components/sass-bootstrap/lib',
          src: ['*.scss'],
          dest: 'css',
          ext: '.css'
        }]
      },
      options: {
        sourceMap: true,
        outputStyle: 'compact',
        //imagePath: "../",
      }
    },

    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: 'css',
          src: ['*.css'],
          dest: 'css-dist',
          ext: '.min.css'
        }]
      }
    },

    jshint: {
      all: ['Gruntfile.js', 'js/theme.js']
    },

    uglify: {
        my_target: {
          files: {
            'js/dist/theme.min.js': ['js/theme.js']
          }
        }
    },

    watch: {
      options: {
        livereload: true
      },

      css1: {
          files: ['css/sass/*.scss', 'style.css'],
          tasks: ['sass:dist']
      },
      css2: {
          files: ['bower_components/sass-bootstrap/lib/*.scss'],
          tasks: ['sass:bootstrap']
      },

      css3: {
        files: ['css/*.css', '!css/*.min.css'],
        tasks: ['cssmin']
      },
      myphp: {
         files: ['*.php', 'inc/*.php', 'template-parts/*.php', 'js/*.js'],
         tasks: ['jshint', 'uglify']
      }
    },

    browserSync: {
        dev: {
            bsFiles: {
                src : [
                'css/**/*.css',
                './**/*.php',
                'js/**/*.js'
                ]
            },
            options: {
              watchTask: true,
                proxy: "im.test",
                  port: 1337 // Change port?

            }
        }
    }
  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');

  grunt.registerTask('default', ['sass', 'cssmin', 'jshint', 'uglify', 'browserSync', 'watch' ]);
};
