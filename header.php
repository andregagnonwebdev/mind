<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mind
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<!-- <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-32x32.png" /> -->
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'mind' ); ?></a>

<div class="container-fluid header">
	<div class="container">
		<header id="masthead" class="site-header row" role="banner">

			<!-- mobile -->
			<div class="site-branding mobile visible-xs visible-sm col-xs-11">
				<?php	// Try to retrieve the Custom Logo
					if ( has_custom_logo()): ?>
						<?php the_custom_logo(); ?>
				<?php endif; ?>
			</div><!-- .site-branding -->
			<div class="site-branding mobile visible-xs visible-sm col-xs-1">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
			</div><!-- .site-branding -->

			<!-- desktop -->
			<div class="site-branding hidden-xs hidden-sm col-sm-12">
				<?php	// Try to retrieve the Custom Logo
					if ( has_custom_logo()): ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="home" rel="home"><?php the_custom_logo(); ?></a>
				<?php endif; ?>
			</div><!-- .site-branding -->



			<nav class="navbar navbar-default col-xs-12" role="navigation">

				<?php
				$menu_args = array(
				"theme_location" => "primary",
				"container_class" => "navbar-collapse collapse",
				"menu_class" => "nav navbar-nav",
				"menu_id" => "main-primary-menu",
				);
				wp_nav_menu( $menu_args);
			?>
			</nav>


			<div class="col-xs-12 search text-right">
				<span class="search-glass"><a><i class="fa fa-search"></i></a></span>
				<span class="list"><a href="<?php echo esc_url( home_url('/issue'))?>"><i class="fa fa-list"></i></a></span>
				<span class="grid"><a href="<?php echo esc_url( home_url('/'))?>"><i class="fa fa-th"></i></a></span>
			</div>

			<div class="search-container col-xs-12 text-right"  style="display:none;">
				<?php get_search_form(); ?>
			</div>

		</header>
	</div><!-- end container -->
</div><!-- end container -->
