<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package mind
 */

get_header(); ?>

<div class="container-fluid">
	<div class="container">

	<div id="primary" class="content-area row">
		<main id="main" class="site-main col-xs-12 col-md-12" role="main">
			<section class="error-404 not-found">
				<header class="page-headerx">
					&nbsp;<br />
					<!-- <h2 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'mind' ); ?></h2> -->
				</header> <!-- .page-header -->

				<div class="row">

					<div class="col-xs-12 col-md-6">
						<div class="page-content">
							<p><?php esc_html_e( '“Not finding what one is seeking but rather finding what was not sought is considered part of the Buddhist path.”', 'mind' ); ?></p>
							<p><?php esc_html_e( '—Patrick McMahon, Fall 2012 “Dharma Jewel.” ', 'mind' ); ?></p>
							<p><?php esc_html_e( 'We can’t find the page you are looking for. Please explore CONTRIBUTORS, CONTENTS or TOPICS in the menu bar above, or type a name, title or phrase into the search box', 'mind' ); ?></p>
							<?php	get_search_form(); ?>
						</div><!-- .page-content -->
					</div>

					<div class="col-xs-12 col-md-6">
						<img src="<?php echo get_field( '404_image', 'option'); ?>" />
					</div>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

</div>
</div>

<?php
get_footer();
