<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>

<?php $post = get_sub_field( 'article'); ?>
<?php //echo $f->post_title; ?>

<?php if ( $post): ?>
	<?php
		// override post_type_name
		setup_postdata( $post);

		//get_template_part( 'template-parts/content-table-of-contents-summary' );

		// check if the flexible content field has rows of data
		if( have_rows('flexible_content') ):
					 // loop through the rows of data
					while ( have_rows('flexible_content') ) : the_row();
						switch( get_row_layout()) {

							case 'title_authors':
								get_template_part( 'template-parts/layout-issue-article-title' );
								break;
							case 'interviewees':
								//get_template_part( 'template-parts/layout-issue-article-interviewees' );
								break;
							default:
								break;
						}
					endwhile;

		else :
						// no layouts found
						?>
						<?php  get_template_part( 'template-parts/layout-default' ); ?>
						<?php
		endif;

		wp_reset_postdata();
	?>
<?php endif; ?>
