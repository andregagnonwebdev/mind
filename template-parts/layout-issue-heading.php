<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>
<div class="container-fluid issue-heading">
	<div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content row">
				<a href="<?php echo esc_url( get_permalink()); ?>">
				<div class="col-xs-12 col-sm-4 text-left title">
						<?php echo get_sub_field( 'title'); ?>
				</div>
				<div class="hidden-xs col-sm-4 text-center season">
						<?php echo get_sub_field( 'season'); ?>&nbsp;&nbsp; Vol. <?php echo get_sub_field( 'volume'); ?> #<?php echo get_sub_field( 'number'); ?>
				</div>
				<div class="hidden-xs col-sm-4 text-right toc-icon">
					<i class="fa fa-list-ul"></i>
				</div>

				<div class="visible-xs col-xs-10 text-left season">
						<?php echo get_sub_field( 'season'); ?>&nbsp;&nbsp; Vol. <?php echo get_sub_field( 'volume'); ?> #<?php echo get_sub_field( 'number'); ?>
				</div>
				</a>
				<div class="visible-xs col-xs-2 text-right toc-icon">
						<i class="fa fa-list-ul"></i>
				</div>

			</div><!-- .entry-content -->
		</article><!-- #post-## -->
	</div>
</div>
