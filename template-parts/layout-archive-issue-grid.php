<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>
<!-- count the number of articles per issue -->
<?php $num_articles = mind_count_articles( $post->ID); ?>

<?php	if ( has_post_thumbnail() ): ?>
	<div class="col-xs-6 col-sm-4 col-md-3 text-left item">
		<a href="<?php echo get_permalink(); ?>" title="<?php echo get_sub_field( 'season'); ?>">
    	<?php echo get_the_post_thumbnail( $post->ID, 'medium' ); ?>
		</a>
		<h5 class="text-center" style="color:white;margin:-15px 0 15px 0;font-size:12px;"><?php echo get_sub_field( 'season'); ?> &nbsp;<?php echo (( $num_articles > 0) ? " (".$num_articles.")" : '' )?></h5>
	</div>
<?php endif; ?>
