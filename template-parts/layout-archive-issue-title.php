<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>

<div class="container-fluid issue-title">
	<div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content row">
				<div class="col-xs-4 col-sm-3 col-md-2 text-left">
					<?php	if ( has_post_thumbnail() ): ?>
						<a href="<?php echo get_permalink(); ?>">
							<div class="visible-xs">
			        	<?php echo get_the_post_thumbnail( $post->ID, 'issue-list' ); ?>
							</div>
							<div class="hidden-xs">
			        	<?php echo get_the_post_thumbnail( $post->ID, 'issue-list' ); ?>
							</div>
						</a>
					<?php endif; ?>
				</div>
				<div class="col-xs-8 col-sm-9 col-md-4 text-left title">
					<a href="<?php echo get_permalink(); ?>"><h2><?php echo get_sub_field( 'title'); ?></h2></a>
					<h3><?php echo get_sub_field( 'season'); ?> </h3>
					<h4>Volume <?php echo get_sub_field( 'volume'); ?>, Number <?php echo get_sub_field( 'number'); ?></h4>
					<h4 class="completed"><?php echo (get_sub_field( 'completed')) ? '' : 'Coming soon'; ?></h4>
				</div>
				<div class="hidden-xs hidden-sm col-md-6 text-left summary">
					<!-- <?php echo get_sub_field( 'summary'); ?> -->
					<?php  echo wp_trim_words( get_sub_field( 'summary'), 50, '<a href="'. esc_url( get_permalink() ) . '">' . '&nbsp;&hellip; ' . __( ' &nbsp;&raquo;', 'mind' ) . '</a>'); ?>

				</div>



			</div><!-- .entry-content -->
			<hr />
		</article><!-- #post-## -->

	</div>
</div>
