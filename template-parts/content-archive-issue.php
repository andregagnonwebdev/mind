<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>


<?php


// check if the flexible content field has rows of data
if( have_rows('flexible_content_issue') ):
			 // loop through the rows of data
			while ( have_rows('flexible_content_issue') ) : the_row();
				switch( get_row_layout()) {

					case 'issue_title':
						get_template_part( 'template-parts/layout-archive-issue-title' );
					break;
					default:
					break;
				}
			endwhile;

else :
				// no layouts found
				?>
				<?php  //get_template_part( 'template-parts/layout-default' ); ?>
				<?php
endif;

//			get_template_part( 'template-parts/content', 'page-flexible' );

?>
