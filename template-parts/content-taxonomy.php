<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>


<?php
// check if the flexible content field has rows of data
// articles
if( have_rows('flexible_content') ):
			 // loop through the rows of data
			while ( have_rows('flexible_content') ) : the_row();
				switch( get_row_layout()) {

					case 'title_authors':
						get_template_part( 'template-parts/layout-taxonomy-article-title' );
						break;
					case 'table_of_contents_summary':
						get_template_part( 'template-parts/layout-taxonomy-article-summary' );
						break;
					default:
						break;
				}
			endwhile;
endif;
?>
