<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>

<div class="container-fluid article-1-column">
	<div class="container">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content row">

				<div class="entry-contentx col-xs-12 col-md-9 col-lg-8 content">
						<?php echo get_sub_field( 'content');	?>
					<?php // the_content(); ?>
				</div><!-- .entry-content -->

				<div class="hidden-xs hidden-sm col-md-3 col-lg-4 keyword">


					<!-- display topics if exist -->
					<?php $topics = get_the_term_list( $post->ID, 'keyword', '', ', ' ); ?>
					<?php if ( $topics): ?>
						<h2 class="sidebar-heading">Topics</h2>
							<div class="topics"><?php echo $topics; ?></div>
							<br /><br />
					<?php endif; ?>

					<?php if ( has_post_thumbnail( $post->ID )): ?>
						<div class="featured-image">
								<?php echo get_the_post_thumbnail( $post->ID); ?>
						</div>
					<?php endif; ?>

					<!-- display author description, if exists -->
					<?php
						$authors = get_the_terms( $post->ID, 'authors');
						$desc = array();
						if ( $authors) {
							foreach ($authors as $a) {
								//var_dump( $a);
								if ( $a->description)
									$desc[] = $a->description;
							}
						}
					?>
					<?php if ( $desc): ?>
						<h2 class="sidebar-heading">Author</h2>
						<?php foreach ($desc as $d): ?>
							<div class="author"><?php echo $d; ?></div>
							<br />
						<?php endforeach; ?>
					<?php endif; ?>

					<!-- display artist description, if exists -->
					<?php
						$artists = get_the_terms( $post->ID, 'artists');
						$desc = array();
						if ( $artists) {
							foreach ($artists as $a) {
								//var_dump( $a);
								if ( $a->description)
									$desc[] = $a->description;
							}
						}
					?>
					<?php if ( $desc): ?>
						<h2 class="sidebar-heading">Artist</h2>
						<?php foreach ($desc as $d): ?>
							<div class="artist"><?php echo $d; ?></div>
							<br />
						<?php endforeach; ?>
					<?php endif; ?>

				</div>

				<div class="visible-xs visible-sm col-xs-12 keyword">

					<!-- display author description, if exists -->
					<?php
						$authors = get_the_terms( $post->ID, 'authors');
						$desc = array();
						if ( $authors) {
							foreach ($authors as $a) {
								//var_dump( $a);
								if ( $a->description)
									$desc[] = $a->description;
							}
						}
					?>
					<?php if ( $desc): ?>
						<h2 class="sidebar-heading">Author</h2>
						<?php foreach ($desc as $d): ?>
							<div class="author"><?php echo $d; ?></div>
							<br />
						<?php endforeach; ?>
					<?php endif; ?>

					<?php if ( has_post_thumbnail( $post->ID )): ?>
						<div class="featured-image">
								<?php echo get_the_post_thumbnail( $post->ID); ?>
						</div>
					<?php endif; ?>

					<?php
						$artists = get_the_terms( $post->ID, 'artists');
						$desc = array();
						if ( $artists) {
							foreach ($artists as $a) {
								//var_dump( $a);
								if ( $a->description)
									$desc[] = $a->description;
							}
						}
					?>
					<?php if ( $desc): ?>
						<h2 class="sidebar-heading">Artist</h2>
						<?php foreach ($desc as $d): ?>
							<div class="artist"><?php echo $d; ?></div>
							<br />
						<?php endforeach; ?>
					<?php endif; ?>

				</div>

			</div><!-- .entry-content -->

		</article><!-- #post-## -->

	</div>
</div>
