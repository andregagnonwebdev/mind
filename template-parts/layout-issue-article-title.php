<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>

<div class="container-fluid issue-article-title">
	<div class="container">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content row">

				<div class="col-xs-12">
					<div class="row" >

						<!-- <div class="col-xs-12 text-right department" style="padding:0;">
							<?php echo get_the_term_list( $post->ID, 'departments', '', ', ' ); ?>
						</div> -->
						<div class="col-xs-12 col-sm-6">
							<a href="<?php the_permalink(); ?>">
								<h4 class="article-title"><?php echo get_sub_field( 'title'); ?></h4>
							</a>

							<?php $by_tag = get_sub_field( 'book_review') ? 'Reviewed By' : 'By'; ?>

							<?php if ( $s = get_the_term_list( $post->ID, 'authors', '', ', ' )): ?>
								<h5 class="article-author"><?php echo $by_tag; ?>  <?php echo $s; ?></h5>
							<?php endif; ?>
						</div>
						<div class="hidden-xs col-sm-6 excerpt">
							<?php if( $summary = get_sub_field( 'summary')): ?>
								<?php	echo $summary; ?>
								<!-- <a style="display:inline;" href="<?php echo esc_url( get_permalink() )?>">&nbsp;&hellip;&nbsp;&raquo;</a> -->
							<?php else: ?>
								<?php	echo wp_trim_words( get_the_excerpt(), 22, '&nbsp;&hellip;');	?>
								<?php	//echo wp_trim_words( get_the_excerpt(), 22, '<a href="'. esc_url( get_permalink() ) . '">' . '&nbsp;&hellip; ' . __( ' &nbsp;&raquo;', 'mind' ) . '</a>');	?>
							<?php endif; ?>
						</div>

						<div class="col-xs-12 rule">
							<hr />
						</div>

					</div>

				</div>

			</div><!-- .entry-content -->

		</article><!-- #post-## -->

	</div>
</div>
