<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>

<div class="container-fluid issue-title">
	<div class="container">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content row">

				<div class="col-xs-12 col-md-6 col-lg-6 text-left">
					<h4><?php echo get_sub_field( 'season'); ?> (Vol.<?php echo get_sub_field( 'volume'); ?> #<?php echo get_sub_field( 'number'); ?>)</h4>
					<h2 class="issue-name"><?php echo get_sub_field( 'title'); ?></h2>
				</div>

				<div class="col-xs-12 col-md-6 col-lg-6 text-right">
					<?php
					if ( has_post_thumbnail() ) {
			        echo get_the_post_thumbnail( $post->ID, 'full' );
			    }
					?>
				</div>

			</div><!-- .entry-content -->

		</article><!-- #post-## -->

	</div>
</div>
