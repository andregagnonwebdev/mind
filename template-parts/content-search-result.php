<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>

<div class="container-fluid">
	<div class="container">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content row">

				<div class="col-xs-12">
					<div class="row" >

						<!-- <div class="col-xs-12 text-left departmentx" style="padding:15px 0 0 0;">
							<?php echo get_the_term_list( $post->ID, 'departments', '', ', ' ); ?>
						</div> -->

						<div class="col-xs-12">

								<a class="title" href="<?php echo esc_url( get_permalink( $post->ID) ); ?>"><h3 style="display:inlinex;"><?php echo get_sub_field( 'title'); ?></h3></a>
								<span style="display:inlinex;">

								<?php
									// get issue, make this into a utility function
									$term_list = wp_get_post_terms( $post->ID, 'issues', array( 'fields' => 'names' ) );
									$issues_tax = $term_list[ 0];
									$issue_list = get_posts( array(
									   'post_type'      => 'issue',
									   'issues'          => $issues_tax,
									 ) );
									 	global $post;
									 	$post = $issue = $issue_list[ 0];
								?>
								<?php setup_postdata( $post); ?>
								<?php
									// check if the flexible content field has rows of data
									if( have_rows('flexible_content_issue') ):
												 // loop through the rows of data
												while ( have_rows('flexible_content_issue') ) : the_row();
													switch( get_row_layout()) {
														case 'issue_title':
															echo ''. get_sub_field( 'title');
															echo ' - '. get_sub_field( 'season');
														break;
													}
												endwhile;
											endif;
								 ?>
								 <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>


							<?php if ( $s = get_the_term_list( $post->ID, 'authors', '', ', ' )): ?>
								- by <?php echo $s; ?>
							<?php endif; ?>
							</span>

							<div class="entry-summary">
								<?php the_excerpt(); ?>
							</div>

							<hr />

						</div>

						<!-- <div class="col-xs-12 text-right keywordx">
							<?php echo get_the_term_list( $post->ID, 'keyword', '', ', ' ); ?>
						</div> -->

					</div>

				</div>

			</div><!-- .entry-content -->
		</article><!-- #post-## -->

	</div>
</div>
