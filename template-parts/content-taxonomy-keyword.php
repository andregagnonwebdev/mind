<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>

<div class="container-fluid taxonomy-keyword">
	<div class="container">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content row">
				<div class="col-xs-12">
					<h1>Topics</h1>
					<?php
						$terms = get_terms( array(
							'taxonomy' => 'keyword',
							'hide_empty' => true,
							'orderby' => 'title',
							'order' => 'ASC'
							)  );

						if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
						    $count = count( $terms );


						// index by alphabet
						$alpha = 'a b c d e f g h i j k l m n o p q r s t u v w x y z';
						$alpha = explode( ' ', $alpha);

						foreach ( $alpha as $a ) {
						    $i = 0;
						    $term_list = '<div class="row keyword">';
						    foreach ( $terms as $term ) {
										$t = strtolower( $term->name);
										if ( $a == $t[0]) {
											$i++;
											$term_count_str = ( $term->count > 1 ) ? (' ('. $term->count .')') : '';

											$term_list .= '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">';
							        $term_list .= '<a href="' . esc_url( get_term_link( $term ) ) . '" alt="' . esc_attr( sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $term->name ) ) . '">' . $term->name
												. $term_count_str . '</a>';  // show keyword count
											$term_list .= '</div>';

							        // if ( $count != $i ) {
						          //   $term_list .= ' &nbsp;&nbsp; ';
							        // }
										}
						    }
								$term_list .= '</div><br />';

								if ( $i ) {
									echo '<div class="letter">' . strtoupper( $a). "</div>";
						    	echo $term_list;
								}
							}
						}

						?>
				</div>
			</div><!-- .entry-content -->

		</article><!-- #post-## -->

	</div>
</div>
