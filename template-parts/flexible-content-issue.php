<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>



<?php
// check if the flexible content field has rows of data
if( have_rows('flexible_content_issue') ):
			 // loop through the rows of data
			while ( have_rows('flexible_content_issue') ) : the_row();
				switch( get_row_layout()) {

					case 'issue_title':
						get_template_part( 'template-parts/layout-issue-heading' );
						get_template_part( 'template-parts/layout-issue-intro' );
						// get_template_part( 'template-parts/layout-issue-toc-heading' );
					break;
					case 'issue_article':
						get_template_part( 'template-parts/layout-issue-article' );
					break;
					case 'issue_section_heading':
						get_template_part( 'template-parts/layout-issue-section-heading' );
					break;
					default:
						//if ( WP_DEBUG) echo 'Layout not found.';
					break;
				}
			endwhile;

else :
				// no layouts found
				?>
				<?php  //get_template_part( 'template-parts/layout-default' ); ?>
				<?php
endif;

//			get_template_part( 'template-parts/content', 'page-flexible' );

?>
