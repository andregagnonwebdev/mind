<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>

<div class="container-fluid departments">
	<div class="container">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content row">
				<div class="col-xs-12">
					<h1>Contents</h1>
					<ul>
						<?php wp_list_categories(
								array('taxonomy' => 'departments',
											'title_li' => '',
											'exclude' => array(360, 680),
											'orderby' => 'menu_order'

							)
							);
						?>
					</ul>
					<?php
						$terms = get_terms( array(
							'taxonomy' => 'departments',
							'hide_empty' => false,
							)  );

							if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
							    $count = count( $terms );
							    $i = 0;
							    $term_list = '<p class="my_term-archive">';
							    foreach ( $terms as $term ) {
							        $i++;
							        $term_list .= '<a href="' . esc_url( get_term_link( $term ) ) . '" alt="' . esc_attr( sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $term->name ) ) . '">' . $term->name . '</a>';
							        if ( $count != $i ) {
							            $term_list .= ' <br /> ';
							        }
							        else {
							            $term_list .= '</p>';
							        }
							    }
							    //echo $term_list;
							}
						?>
				</div>
			</div><!-- .entry-content -->

		</article><!-- #post-## -->

	</div>
</div>
