<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>

<div class="container-fluid">
	<div class="container">
		<hr  />

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content row">
				<div class="col-xs-12">
					<?php $anchor = ( isset( $artists) ) ? '#artist-image' : ''; ?>
					<a href="<?php the_permalink(); ?><?php echo $anchor; ?>">
						<h4><?php echo get_sub_field( 'title'); ?></h4>
					</a>
				</div>
				<div class="col-xs-12">
					<?php if ( $s = get_the_term_list( $post->ID, 'authors', '', ', ' )): ?>
						<h5>By <?php echo $s; ?></h5>
					<?php endif; ?>
				</div>
			</div><!-- .entry-content -->

		</article><!-- #post-## -->

	</div>
</div>
