<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>

<div class="container-fluid taxonomy-keyword">
	<div class="container">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content row">
				<div class="col-xs-12">
					<h1>Writers</h1>
					<?php
						$terms = get_terms( array(
							'taxonomy' => 'authors',
							'hide_empty' => false,
							)  );

						if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
						    $count = count( $terms );

							$alpha_term = array();
							foreach ( $terms as $key => $term ) {
								$t = strtolower( $term->name);
								$names = explode( ' ', $t);
								$size = count( $names);
								$value = $names[ $size - 1];
								$alpha_term[ $key] = $value;
							}
							array_multisort( $alpha_term, SORT_ASC, $terms);

						// index by alphabet
						$alpha = 'a b c d e f g h i j k l m n o p q r s t u v w x y z';
						$alpha = explode( ' ', $alpha);

						foreach ( $alpha as $a ) {
						    $i = 0;
						    $term_list = '<div class="row keyword">';
						    foreach ( $terms as $key => $term ) {
										$t = $alpha_term[ $key];
										if ( $a == $t[0]) {
											$i++;
											$term_count_str = ( $term->count > 1 ) ? (' ('. $term->count .')') : '';
											$term_list .= '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">';
							        $term_list .= '<a href="' . esc_url( get_term_link( $term ) ) . '" alt="' . esc_attr( sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $term->name ) ) . '">' . $term->name
												. $term_count_str . '</a>';  // show keyword count
											$term_list .= '</div>';

							        // if ( $count != $i ) {
						          //   $term_list .= ' &nbsp;&nbsp; ';
							        // }
										}
						    }
								$term_list .= '</div><br />';

								if ( $i ) {
									echo '<div class="letter">' . strtoupper( $a). "</div>";
						    	echo $term_list;
								}
							}
						}

						?>
				</div>
			</div><!-- .entry-content -->

		</article><!-- #post-## -->

	</div>
</div>
