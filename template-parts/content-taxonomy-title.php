<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>

<div class="container-fluid">
	<div class="container">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content row">
				<div class="col-xs-12">
					<?php 	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
					<h2><?php echo ucfirst( $term->taxonomy); ?>: <?php echo $term->name; ?></h2>
				</div>
			</div><!-- .entry-content -->

		</article><!-- #post-## -->

	</div>
</div>
