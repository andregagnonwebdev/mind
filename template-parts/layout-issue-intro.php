<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>
<div class="container-fluid issue-intro hidden-xs">
	<div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content row">
				<div class="col-sm-3 cover">
					<?php echo get_the_post_thumbnail( $post->ID, 'medium' ); ?>
				</div>
				<div class="hidden-xs col-sm-8 intro">
						<h2><?php echo get_sub_field( 'title'); ?></h2>
						<!-- <h4><?php echo get_sub_field( 'season'); ?>&nbsp;&nbsp; Vol. <?php echo get_sub_field( 'volume'); ?> #<?php echo get_sub_field( 'number'); ?></h4> -->
						<p><?php echo get_sub_field( 'summary'); ?></p>

				</div>


			</div><!-- .entry-content -->
		</article><!-- #post-## -->
	</div>
</div>
