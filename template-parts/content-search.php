<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>


		<?php
		// check if the flexible content field has rows of data
		if( have_rows('flexible_content') ):
					 // loop through the rows of data
					while ( have_rows('flexible_content') ) : the_row();
						switch( get_row_layout()) {

							case 'title_authors':
								get_template_part( 'template-parts/content-search-result' );
								//get_template_part( 'template-parts/content-title-authors' );
								break;
							default:
							break;
						}
					endwhile;
					?>
			<?php
				endif;
			?>
