<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>



<?php
// check if the flexible content field has rows of data
if( have_rows('flexible_content') ):
			 // loop through the rows of data
			while ( have_rows('flexible_content') ) : the_row();
				switch( get_row_layout()) {

					case 'title_authors':
						get_template_part( 'template-parts/layout-article-title-authors' );
						break;
					case '1_column':
						get_template_part( 'template-parts/layout-article-1-column' );
					break;
					case 'divide':
						get_template_part( 'template-parts/layout-article-divide' );
						break;
					default:
						//if ( WP_DEBUG) echo 'Layout not found.';
						break;
				}
			endwhile;

else :
				// no layouts found
				?>
				<?php  get_template_part( 'template-parts/layout-default' ); ?>
				<?php
endif;

//			get_template_part( 'template-parts/content', 'page-flexible' );

?>
