<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>
<div class="container-fluid im-footer">
	<div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<div class="entry-content row">

					<div class="col-xs-12">
						<span class="heading"><?php echo get_sub_field( 'text'); ?></span>
					</div>

			</div><!-- .entry-content -->
		</article><!-- #post-## -->
	</div>
</div>
