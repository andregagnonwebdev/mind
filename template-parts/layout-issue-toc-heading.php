<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>
<div class="container-fluid issue-intro">
	<div class="container">
		<article>
			<div class="entry-content row">
				<div class="col-xs-12 toc">
					<h4 class="heading">table of contents</h4>
				</div>
			</div><!-- .entry-content -->
		</article><!-- #post-## -->
	</div>
</div>
