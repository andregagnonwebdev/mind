<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>

<div class="container-fluid article-divide">
	<div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content row">
				<div class="entry-content col-xs-12">
					<hr />
				</div><!-- .entry-content -->
			</div><!-- .entry-content -->
		</article><!-- #post-## -->
	</div>
</div>
