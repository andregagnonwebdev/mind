<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>

<div class="container-fluid">
	<div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content row">
				<div class="col-xs-12">
					<p><?php echo get_sub_field( 'summary'); ?></p>
				</div>
			</div><!-- .entry-content -->

		</article><!-- #post-## -->

	</div>
</div>
