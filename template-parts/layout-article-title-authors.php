<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mind
 */

?>
<?php
	$term_list = wp_get_post_terms( $post->ID, 'issues', array( 'fields' => 'names' ) );
	$issues_tax = $term_list[ 0];
	$issue_list = get_posts( array(
	   'post_type'      => 'issue',
	   'issues'          => $issues_tax,
	 ) );
	 	global $post;
	 	$post = $issue = $issue_list[ 0];
?>
<?php setup_postdata( $post); ?>
<?php
	// check if the flexible content field has rows of data
	if( have_rows('flexible_content_issue') ):
				 // loop through the rows of data
				while ( have_rows('flexible_content_issue') ) : the_row();
					switch( get_row_layout()) {
						case 'issue_title':
							get_template_part( 'template-parts/layout-issue-heading' );
						break;
					}
				endwhile;
			endif;
 ?>
 <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>



<div class="container-fluid article-title-author">
	<div class="container">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content row">

				</div>
				<div class="col-xs-12">
					<div class="row" >

						<div class="col-xs-12 col-md-9 col-lg-8">
							<!-- tbd put terms in correct order -->
							<!-- <p class="department-title"><?php //echo get_the_term_list( $post->ID, 'departments', '', ' | ' ); ?></p> -->
							<p class="department-title"><?php echo mind_get_the_term_list( $post->ID, 'departments' ); ?></p>

							<h2><?php echo get_sub_field( 'title'); ?></h2>

							<?php $by_tag = get_sub_field( 'book_review') ? 'Reviewed By' : 'By'; ?>
							<?php if ( $s = get_the_term_list( $post->ID, 'authors', '', ', ' )): ?>
								<h3><?php echo $by_tag; ?> <?php echo $s; ?></h3>
							<?php endif; ?>

							<!-- ACF method -->
							<?php
							if( have_rows('authors') ):
								?>
								<!-- <h3>By -->
									<?php
									$i = 0; $s = '';
									while ( have_rows('authors') ) : the_row();
									?>
										<?php if ( $i): ?>
											<?php $s .= ', '; ?>
										<?php endif; ?>
										<?php $s .= get_sub_field( 'name');?>
										<?php $i++; ?>
									<?php
									endwhile;
		//							echo $s;
									?>
								<!-- </h3> -->
								<?php
							endif;
							?>


						</div>
						<div class="col-xs-12 col-md-3 col-lg-4">
							&nbsp;
						</div>

						<!-- <div class="col-xs-12 text-right keyword">
							<?php echo get_the_term_list( $post->ID, 'keyword', '', ', ' ); ?>
						</div> -->

					</div>

				</div>
				<div class="hidden-xs hidden-sm col-md-2 col-lg-3">
					&nbsp;
				</div>




			</div><!-- .entry-content -->

		</article><!-- #post-## -->

	</div>
</div>
